# 实验3：创建分区表

- 学号：202010414403，姓名：邓雨桐，班级：软工20-4班

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

- **步骤1：使用Sales用户创建ORDERS和Order_details两张表**

  ```sql
  CREATE TABLE orders 
  (
   order_id NUMBER(9, 0) NOT NULL
   , customer_name VARCHAR2(40 BYTE) NOT NULL 
   , customer_tel VARCHAR2(40 BYTE) NOT NULL 
   , order_date DATE NOT NULL 
   , employee_id NUMBER(6, 0) NOT NULL 
   , discount NUMBER(8, 2) DEFAULT 0 
   , trade_receivable NUMBER(8, 2) DEFAULT 0 
   , CONSTRAINT ORDERS_PK PRIMARY KEY 
    (
      ORDER_ID 
    )
  ) 
  TABLESPACE USERS 
  PCTFREE 10 INITRANS 1 
  STORAGE (   BUFFER_POOL DEFAULT ) 
  NOCOMPRESS NOPARALLEL 
  
  PARTITION BY RANGE (order_date) 
  (
   PARTITION PARTITION_BEFORE_2016 VALUES LESS THAN (
   TO_DATE(' 2016-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
   'NLS_CALENDAR=GREGORIAN')) 
   NOLOGGING
   TABLESPACE USERS
   PCTFREE 10 
   INITRANS 1 
   STORAGE 
  ( 
   INITIAL 8388608 
   NEXT 1048576 
   MINEXTENTS 1 
   MAXEXTENTS UNLIMITED 
   BUFFER_POOL DEFAULT 
  ) 
  NOCOMPRESS NO INMEMORY  
  , PARTITION PARTITION_BEFORE_2020 VALUES LESS THAN (
  TO_DATE(' 2020-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
  'NLS_CALENDAR=GREGORIAN')) 
  NOLOGGING
  TABLESPACE USERS
  , PARTITION PARTITION_BEFORE_2021 VALUES LESS THAN (
  TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 
  'NLS_CALENDAR=GREGORIAN')) 
  NOLOGGING 
  TABLESPACE USERS
  );
  --以后再逐年增加新年份的分区
  ALTER TABLE orders ADD PARTITION partition_before_2022
  VALUES LESS THAN(TO_DATE('2022-01-01','YYYY-MM-DD'))
  TABLESPACE USERS;
  
  CREATE TABLE order_details
  (
  id NUMBER(9, 0) NOT NULL 
  , order_id NUMBER(10, 0) NOT NULL
  , product_id VARCHAR2(40 BYTE) NOT NULL 
  , product_num NUMBER(8, 2) NOT NULL 
  , product_price NUMBER(8, 2) NOT NULL 
  , CONSTRAINT ORDER_DETAILS_PK PRIMARY KEY 
    (
      id 
    )
  , CONSTRAINT order_details_fk1 FOREIGN KEY  (order_id)
  REFERENCES orders  (  order_id   )
  ENABLE
  ) 
  TABLESPACE USERS 
  PCTFREE 10 INITRANS 1 
  STORAGE ( BUFFER_POOL DEFAULT ) 
  NOCOMPRESS NOPARALLEL
  PARTITION BY REFERENCE (order_details_fk1);
  
  ```

- **步骤2：创建序列SEQ1**

  ```sql
  CREATE SEQUENCE  SEQ1  MINVALUE 1 MAXVALUE 999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
  
  ```

- **步骤3：扩展表空间**

  在插入数据时，提示表空间不足，因此需要扩展表空间。

  切换到用户sysdba，使用下列查询语句：

  ```sql
  ALTER TABLESPACE users
  ADD DATAFILE '/home/oracle/datafile.dbf' SIZE 1G;
  ```

- **步骤4：给orders表的customer_name列增加B-Tree索引**

  ```sql
  CREATE INDEX idx_customer_name
  ON sale.orders (customer_name)
  TABLESPACE users;
  ```

- **步骤5：创建表序列**

  ```sql
  -- 创建orders表的序列
  CREATE SEQUENCE orders_seq
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;
  
  -- 创建order_details表的序列
  CREATE SEQUENCE order_details_seq
  START WITH 1
  INCREMENT BY 1
  NOCACHE
  NOCYCLE;
  
  ```

  

- **步骤6：插入数据**

  ```sql
  -- 插入 4000 条数据
  INSERT INTO orders (order_id, customer_name, customer_tel, order_date, employee_id, discount, trade_receivable)
  SELECT SEQ1.NEXTVAL, 'Customer ' || ROWNUM, 'Tel ' || ROWNUM, TO_DATE('2022-01-01', 'YYYY-MM-DD') - DBMS_RANDOM.VALUE(1, 365),
         TRUNC(DBMS_RANDOM.VALUE(1, 100)), TRUNC(DBMS_RANDOM.VALUE(0, 50)), TRUNC(DBMS_RANDOM.VALUE(100, 10000))
  FROM DUAL
  CONNECT BY ROWNUM <= 4000;
  INSERT INTO order_details (id, order_id, product_id, product_num, product_price)
  SELECT SEQ1.NEXTVAL, o.order_id, 'Product ' || ROWNUM, TRUNC(DBMS_RANDOM.VALUE(1, 10)), TRUNC(DBMS_RANDOM.VALUE(10, 1000))
  FROM orders o
  WHERE o.order_date < TO_DATE('2022-01-01','YYYY-MM-DD')
  CONNECT BY ROWNUM <= 20000;
  ```
  
  此 SQL 语句将在 orders 表中插入 4000 行数据，其中 order_id 由 SEQ1 序列生成，customer_name 和 customer_tel 为随机生成的字符串，order_date 为 2022 年 1 月 1 日之前的某个随机日期，employee_id 为 1 到 100 中的一个随机整数，discount 为 0 到 50 中的一个随机整数，trade_receivable 为 100 到 10000 中的一个随机整数。
  
  此 SQL 语句将在 `order_details` 表中插入 20000 行数据，其中 `id` 由 `SEQ1` 序列生成，`order_id` 为 1 到 4000 中的一个随机整数，`product_id` 为随机生成的字符串，`product_num` 为 1 到 10 中的一个随机整数，`product_price` 为 10 到 1000 中的一个随机整数。



![image-20230508215113669](./image-20230508215113669.png)

![image-20230508215132855](./image-20230508215132855.png)



- **步骤7：查询数据**

```sql
SELECT o.order_id, o.customer_name, od.product_id, od.product_num, od.product_price
FROM orders o
JOIN order_details od
ON o.order_id = od.order_id
WHERE o.order_date BETWEEN TO_DATE('2021-01-01', 'YYYY-MM-DD') AND TO_DATE('2021-12-31', 'YYYY-MM-DD')
AND od.product_price >= 500;
```

![image-20230508220005789](C:\Users\MrBear\Desktop\test2\image-20230508220005789.png)

## 结论

- 1、对于数据量较大的表，进行分区可以大幅提升查询效率，特别是在按照分区键进行查询时，可以直接跳过不必要的分区，减少查询的数据量，提高查询速度。
- 2、对于插入操作来说，分区表的插入速度要比不分区的表慢，因为插入数据时需要确定数据应该插入到哪个分区，这增加了插入操作的开销。
- 3、对于更新和删除操作来说，分区表和不分区表的差别并不明显，因为更新和删除操作通常都是针对少量数据进行的，不会受到分区的影响。
- 4、在进行联合查询时，如果两个表都是分区表，那么查询的效率会更高，因为可以利用分区键进行查询优化；如果只有一个表是分区表，那么查询的效率也会有所提高，但是优化效果并不明显；如果两个表都不是分区表，则不会产生分区查询的优化效果。