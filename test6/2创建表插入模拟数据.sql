--创建名为 users 的表，包含 user_id（6位数字，不能为空）、username（最多20个字符）、email（最多30个字符）和 password（32位字符）列，以 user_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE users (
  user_id NUMBER(6) NOT NULL,
  username VARCHAR2(20),
  email VARCHAR2(30),
  password CHAR(32),
  PRIMARY KEY (user_id)
)
TABLESPACE data_tablespace;
--创建名为 products 的表，包含 product_id（6位数字，不能为空）、product_name（最多50个字符）、category（最多20个字符）和 price（10位数字，2位小数，不能为空）列，以 product_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE products (
  product_id NUMBER(6) NOT NULL,
  product_name VARCHAR2(50),
  category VARCHAR2(20),
  price NUMBER(10,2),
  PRIMARY KEY (product_id)
)
TABLESPACE data_tablespace;

-- 插入5千条商品信息随机数据
DECLARE 
  v_product_id INTEGER;
BEGIN
  FOR i IN 1..5000 LOOP
    v_product_id := i;
    -- 插入一条 product_id、product_name、category 和 price 随机生成的数据
    INSERT INTO products(product_id,product_name,category,price)
    VALUES(v_product_id,'product'||v_product_id, 'cate_'||(MOD(i-1,5)+1), TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100);
  END LOOP;
  COMMIT;
END;
/


--创建名为 addresses 的表，包含 address_id（6位数字，不能为空）、user_id（6位数字，不能为空）、name（最多50个字符）、address（最多200个字符）和 phone（最多50个字符）列，以 address_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE addresses(
  address_id NUMBER(6) NOT NULL,
  user_id NUMBER(6) NOT NULL,
  name VARCHAR2(50),
  address VARCHAR2(200),
  phone VARCHAR2(50),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (address_id)
)
TABLESPACE data_tablespace;

-- 插入5千条客户端地址信息随机数据
DECLARE 
  c_num_users CONSTANT NUMBER := 1000; -- 假设有1000个用户
  v_address_id INTEGER;
  v_user_id INTEGER := 1;
BEGIN
  FOR i IN 1..5000 LOOP
    v_address_id := i;
    
    IF (i mod 5 = 1) THEN -- 每个用户有5条地址信息
      v_user_id := MOD(v_user_id + 1, c_num_users) + 1;
    END IF; 
    --插入一条 address_id、user_id、name、address 和 phone 随机生成的数据
    INSERT INTO addresses(address_id,user_id,name,address,phone)
    VALUES(v_address_id,v_user_id, 'address_'||(MOD(i-1,5)+1), 'addr_'||i||'_lane_XX_street', '88'||DBMS_RANDOM.VALUE(10000000000,99999999999));
  END LOOP;
  COMMIT;
END;
/

--创建名为 cart_items 的表，包含 item_id（6位数字，不能为空）、cart_id（6位数字，不能为空）、product_id（6位数字，不能为空）、quantity（10位数字，不能为空）和 created_date（默认为当前日期时间）列，以 item_id 为主键，存储在 index_tablespace 表空间中
CREATE TABLE cart_items (
  item_id NUMBER(6) NOT NULL,
  cart_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  quantity NUMBER(10),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (item_id)
)
TABLESPACE  index_tablespace;

-- 插入5千条购物项信息随机数据，每个购物车有不同数量的商品
DECLARE 
  c_num_carts CONSTANT NUMBER := 1000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_cart_id INTEGER;
  v_product_id INTEGER;
  v_quantity INTEGER;
BEGIN
  FOR i IN 1..80000 LOOP -- 每个购物车最多拥有20个商品
    v_item_id := i;
    v_cart_id := MOD(i-1,c_num_carts)+1;
    v_product_id := MOD(i-1, c_num_products)+1;    
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    --插入一条 item_id、cart_id、product_id 和 quantity 随机生成的数据
    INSERT INTO cart_items(item_id,cart_id,product_id,quantity)
    VALUES(v_item_id,v_cart_id, v_product_id, v_quantity);
  END LOOP;
  COMMIT;
END;
/

--创建名为 order_items 的表，包含 item_id（6位数字，不能为空）、order_id（6位数字，不能为空）、product_id（6位数字，不能为空）、unit_price（10位数字，2位小数，不能为空）和 quantity（10位数字，不能为空）列，以 item_id 为主键，存储在 index_tablespace 表空间中
CREATE TABLE order_items (
  item_id NUMBER(6) NOT NULL,
  order_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  unit_price NUMBER(10,2),
  quantity NUMBER(10),
  PRIMARY KEY (item_id)
)
TABLESPACE  index_tablespace;

-- 插入5千条订单商品信息随机数据，每个订单有不同数量的商品
DECLARE 
  c_num_orders CONSTANT NUMBER := 5000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_order_id INTEGER;
  v_product_id INTEGER;
  v_unit_price NUMBER(10,2);
  v_quantity INTEGER;  
BEGIN
  FOR i IN 1..25000 LOOP -- 每个订单最多拥有5个商品
    v_item_id := i*5 - DBMS_RANDOM.VALUE(1,5) + 1; -- 随机产生item_id
    v_order_id := MOD(i-1,c_num_orders)+1;
    v_product_id := MOD(v_item_id-1, c_num_products)+1;    
    v_unit_price := TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100;
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    --插入一条 item_id、order_id、product_id、unit_price 和 quantity 随机生成的数据
    INSERT INTO order_items(item_id,order_id,product_id,unit_price,quantity)
    VALUES(v_item_id,v_order_id, v_product_id, v_unit_price, v_quantity);  
    
    IF (MOD(i,5000)=0) THEN -- 每5000条提交一次（或者可以调大）
      COMMIT;
    END IF;
  END LOOP;
END;
/