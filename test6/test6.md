# 实验6：基于Oracle数据库的商品销售系统的设计

## 内容

设计一套基于Oracle数据库的商品销售系统的数据库设计方案。

- 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
- 设计权限及用户分配方案。至少两个用户。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
- 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。

  文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：

  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx]()，学校格式的完整报告。

## **表及表空间设计方案。**

- ### 创建表空间

```sql
/* 创建名为 data_tablespace 的表空间 */
CREATE TABLESPACE data_tablespace
DATAFILE 'dyt_sales_data01.dbf'
SIZE 500M
AUTOEXTEND ON;

/* 创建名为 index_tablespace 的表空间 */
CREATE TABLESPACE index_tablespace
DATAFILE 'dyt_sales_idx01.dbf'
SIZE 500M
AUTOEXTEND ON;

```

![pict01](./pict01.png)

- ### 用户表
- users表：包含用户的基本信息，如user_id、username、email和password。其中，user_id作为主键，不能为空，存储在data_tablespace表空间中。
```sql
--创建名为 users 的表，包含 user_id（6位数字，不能为空）、username（最多20个字符）、email（最多30个字符）和 password（32位字符）列，以 user_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE users (
  user_id NUMBER(6) NOT NULL,
  username VARCHAR2(20),
  email VARCHAR2(30),
  password CHAR(32),
  PRIMARY KEY (user_id)
)
TABLESPACE data_tablespace;

```

![pict02](./pict02.png)

- 商品信息表
- products表：包含商品的基本信息，如product_id、product_name、category和price。其中，product_id作为主键，不能为空，存储在data_tablespace表空间中。我们还通过插入5000条随机数据，为商品信息添加了样本数据。
```sql
--创建名为 products 的表，包含 product_id（6位数字，不能为空）、product_name（最多50个字符）、category（最多20个字符）和 price（10位数字，2位小数，不能为空）列，以 product_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE products (
  product_id NUMBER(6) NOT NULL,
  product_name VARCHAR2(50),
  category VARCHAR2(20),
  price NUMBER(10,2),
  PRIMARY KEY (product_id)
)
TABLESPACE data_tablespace;

-- 插入5千条商品信息随机数据
DECLARE 
  v_product_id INTEGER;
BEGIN
  FOR i IN 1..5000 LOOP
    v_product_id := i;
    -- 插入一条 product_id、product_name、category 和 price 随机生成的数据
    INSERT INTO products(product_id,product_name,category,price)
    VALUES(v_product_id,'product'||v_product_id, 'cate_'||(MOD(i-1,5)+1), TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100);
  END LOOP;
  COMMIT;
END;
/
```

![pict04](./pict04.png)

- 地址表
- addresses表：包含客户端地址信息，如address_id、user_id、name、address和phone。其中，address_id作为主键，不能为空，存储在data_tablespace表空间中。我们通过插入5000条随机数据，为客户地址信息添加了样本数据。
```sql
--创建名为 addresses 的表，包含 address_id（6位数字，不能为空）、user_id（6位数字，不能为空）、name（最多50个字符）、address（最多200个字符）和 phone（最多50个字符）列，以 address_id 为主键，存储在 data_tablespace 表空间中
CREATE TABLE addresses(
  address_id NUMBER(6) NOT NULL,
  user_id NUMBER(6) NOT NULL,
  name VARCHAR2(50),
  address VARCHAR2(200),
  phone VARCHAR2(50),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (address_id)
)
TABLESPACE data_tablespace;

-- 插入5千条客户端地址信息随机数据
DECLARE 
  c_num_users CONSTANT NUMBER := 1000; -- 假设有1000个用户
  v_address_id INTEGER;
  v_user_id INTEGER := 1;
BEGIN
  FOR i IN 1..5000 LOOP
    v_address_id := i;
    
    IF (i mod 5 = 1) THEN -- 每个用户有5条地址信息
      v_user_id := MOD(v_user_id + 1, c_num_users) + 1;
    END IF; 
    --插入一条 address_id、user_id、name、address 和 phone 随机生成的数据
    INSERT INTO addresses(address_id,user_id,name,address,phone)
    VALUES(v_address_id,v_user_id, 'address_'||(MOD(i-1,5)+1), 'addr_'||i||'_lane_XX_street', '88'||DBMS_RANDOM.VALUE(10000000000,99999999999));
  END LOOP;
  COMMIT;
END;
/


```

- 购物车表
- cart_items表：用于存储购物车信息，包含item_id、cart_id、product_id和quantity。其中，item_id作为主键，不能为空，存储在index_tablespace表空间中。我们通过插入5000条随机数据，为购物车信息添加了样本数据。
```sql
--创建名为 cart_items 的表，包含 item_id（6位数字，不能为空）、cart_id（6位数字，不能为空）、product_id（6位数字，不能为空）、quantity（10位数字，不能为空）和 created_date（默认为当前日期时间）列，以 item_id 为主键，存储在 index_tablespace 表空间中
CREATE TABLE cart_items (
  item_id NUMBER(6) NOT NULL,
  cart_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  quantity NUMBER(10),
  created_date DATE DEFAULT SYSDATE,
  PRIMARY KEY (item_id)
)
TABLESPACE  index_tablespace;

-- 插入5千条购物项信息随机数据，每个购物车有不同数量的商品
DECLARE 
  c_num_carts CONSTANT NUMBER := 1000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_cart_id INTEGER;
  v_product_id INTEGER;
  v_quantity INTEGER;
BEGIN
  FOR i IN 1..80000 LOOP -- 每个购物车最多拥有20个商品
    v_item_id := i;
    v_cart_id := MOD(i-1,c_num_carts)+1;
    v_product_id := MOD(i-1, c_num_products)+1;    
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    --插入一条 item_id、cart_id、product_id 和 quantity 随机生成的数据
    INSERT INTO cart_items(item_id,cart_id,product_id,quantity)
    VALUES(v_item_id,v_cart_id, v_product_id, v_quantity);
  END LOOP;
  COMMIT;
END;
/

```

- 订单商品表
- order_items表：用于存储订单商品信息，包含item_id、order_id、product_id、unit_price和quantity。其中，item_id作为主键，不能为空，存储在index_tablespace表空间中。我们通过插入5000条随机数据，为订单商品信息添加了样本数据。
```sql
--创建名为 order_items 的表，包含 item_id（6位数字，不能为空）、order_id（6位数字，不能为空）、product_id（6位数字，不能为空）、unit_price（10位数字，2位小数，不能为空）和 quantity（10位数字，不能为空）列，以 item_id 为主键，存储在 index_tablespace 表空间中
CREATE TABLE order_items (
  item_id NUMBER(6) NOT NULL,
  order_id NUMBER(6) NOT NULL,
  product_id NUMBER(6) NOT NULL,
  unit_price NUMBER(10,2),
  quantity NUMBER(10),
  PRIMARY KEY (item_id)
)
TABLESPACE  index_tablespace;

-- 插入5千条订单商品信息随机数据，每个订单有不同数量的商品
DECLARE 
  c_num_orders CONSTANT NUMBER := 5000;
  c_num_products CONSTANT NUMBER := 5000;
  v_item_id INTEGER;
  v_order_id INTEGER;
  v_product_id INTEGER;
  v_unit_price NUMBER(10,2);
  v_quantity INTEGER;  
BEGIN
  FOR i IN 1..25000 LOOP -- 每个订单最多拥有5个商品
    v_item_id := i*5 - DBMS_RANDOM.VALUE(1,5) + 1; -- 随机产生item_id
    v_order_id := MOD(i-1,c_num_orders)+1;
    v_product_id := MOD(v_item_id-1, c_num_products)+1;    
    v_unit_price := TRUNC(DBMS_RANDOM.VALUE(10,10000)*100)/100;
    v_quantity := DBMS_RANDOM.VALUE(1,10);
    --插入一条 item_id、order_id、product_id、unit_price 和 quantity 随机生成的数据
    INSERT INTO order_items(item_id,order_id,product_id,unit_price,quantity)
    VALUES(v_item_id,v_order_id, v_product_id, v_unit_price, v_quantity);  
    
    IF (MOD(i,5000)=0) THEN -- 每5000条提交一次（或者可以调大）
      COMMIT;
    END IF;
  END LOOP;
END;
/
```

  

## **设计权限及用户，分配方案**

在基于Oracle数据库的商品销售系统中，可以设计以下用户和权限的分配方案：

1. 创建两个角色，分别为销售经理和销售操作员

```sql
-- 创建两个角色，分别为销售经理和销售操作员
CREATE ROLE c##myapp_sales_manager_1;
CREATE ROLE c##myapp_sales_operator_1;
```

2. 创建具有不同权限的用户

```sql
-- 创建用户 Alice，并授予销售经理角色，拥有管理数据表的所有权限
CREATE USER c##alice IDENTIFIED BY password123;
GRANT CONNECT, RESOURCE TO c##alice;
GRANT CREATE SESSION TO c##alice; -- 允许用户登录
GRANT UNLIMITED TABLESPACE TO c##alice; -- 不限制表空间大小
GRANT c##myapp_sales_manager_1 TO c##alice; -- 授予销售经理角色

-- 创建用户 Bob，并授予销售操作员角色，只能查看和修改部分字段
CREATE USER c##bob IDENTIFIED BY password456;
GRANT CONNECT, RESOURCE TO c##bob;
GRANT CREATE SESSION TO c##bob;
GRANT SELECT ON products TO c##bob; -- 允许查询商品信息表
GRANT INSERT, UPDATE (quantity) ON cart_items TO c##bob; -- 允许更改购物项数量
GRANT SELECT ON users TO c##bob ; -- 允许查询用户列表
GRANT c##myapp_sales_operator_1 TO c##bob; -- 授予销售操作员角色
```

![pict03](./pict03.png)

3. 授予权限给角色

```sql
-- 销售经理角色拥有从sales_data表空间读写相关表的权限
GRANT SELECT,INSERT,UPDATE,DELETE ON products TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON addresses TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON carts TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON cart_items TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON orders TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON order_items TO c##myapp_sales_manager_1;

-- 销售操作员角色拥有从sales_data表空间读取相关表的权限
GRANT SELECT ON products TO c##myapp_sales_operator_1;
GRANT SELECT ON addresses TO c##myapp_sales_operator_1;
GRANT SELECT ON users TO c##myapp_sales_operator_1;
GRANT SELECT, INSERT, UPDATE(quantity) ON cart_items TO c##myapp_sales_operator_1;
GRANT SELECT, INSERT, UPDATE(unit_price,quantity) ON order_items TO c##myapp_sales_operator_1;
```



<img src="./pict05.png" alt="pict05" style="zoom:150%;" />

上述是一种简单的用户和权限分配方案，其中alice用户被赋予管理员权限，可以操作所有数据表，在SQL等基本操作中扮演着重要的角色。而bob用户只有在特定情况下才能够执行查询和修改少数字段，并且只被赋予sales_operator角色的静态语句执行权限。这种方式可以有效地保护数据库的敏感信息，避免意外或异常操作误操作。


## **创建程序包**

假设我们正在建立一个在线商店，并且要为用户订单创建一个便利的计算器。
在这个数据库中，我们需要为销售经理和销售操作员两种角色分别创建用户并赋予权限。我们创建了c##myapp_sales_manager_1和c##myapp_sales_operator_1两个角色，分别代表销售经理和销售操作员。我们还创建了Alice和Bob两个用户，分别被授予这两个角色。
对于销售经理，我们授予其管理数据表的所有权限，包括从sales_data表空间读写相关表的权限。对于销售操作员，我们只允许其查询部分表，以及更改部分字段，如购物车的商品数量和订单的商品数量和单价。这样做可以有效控制操作员的权限范围，保证了数据的安全性和完整性。
总的来说，我们通过角色、权限和用户的设计，为销售数据数据库提供了可靠的数据访问控制措施，保证了数据的安全性和规范性。
在此数据库中，我们需要实现三个过程/函数，用于处理购物车和订单相关操作。

首先，我们需要一个名为create_order的函数。此函数将接收一系列参数，包括订单名称、地址、电话、邮箱以及产品数量列表。通过遍历产品数量列表，我们可以在订单项表(order_items)中为每个产品生成新的订单项(order_item)。在生成订单项的同时，我们还需要检查产品的库存量，并计算总价格(total_price)。最后，我们将新地址插入到地址表(addresses)中，并返回总价格。

其次，我们需要一个名为update_product_and_get_report的函数。此函数将接收一系列参数，包括产品ID、新的名称、类别和价格。通过查询旧的产品信息，并更新产品信息，我们可以生成一份更新报告字符串(report_str)，其中包括旧的名称(如果更改）、价格增加/减少和类别从A转变为B的信息。

第三个过程是clear_cart_items存储过程。此存储过程将接收一个购物车ID(cart_id)，并删除与该ID关联的购物车项(cart_items)。这可以通过简单的SQL语句实现。

这些过程/函数将为我们的购物车和订单系统提供必要的功能，并确保数据的完整性和安全性。我们可以通过使用角色、权限和用户来管理这些过程/函数的访问，以保护数据的安全性。

假设我们正在建立一个在线商店，并且要为用户订单创建一个便利的计算器。

1. create_order 函数

此函数将接收以下参数:name,address,phone,email,和prod_qty_array(单独的产品ID和相应数量的值列表)。然后处理传入的数据，并执行以下操作:

- 创建新的订单(order_id)
- 将新地址插入到addresses表
- 为每个商品生成新的order_item
- 返回总价格(total_price)

```sql
CREATE OR REPLACE PACKAGE body shopping_cart_pkg IS

FUNCTION create_order (
name IN VARCHAR2,
address IN VARCHAR2,
phone IN VARCHAR2,
email IN VARCHAR2,
prod_qty_array IN sys.odcinumberlist
) RETURN NUMBER IS

new_order_id NUMBER;
new_address_id NUMBER;
total_price NUMBER := 0;
current_quantity NUMBER:=0;
BEGIN

-- 生成新的订单ID
SELECT order_seq.NEXTVAL INTO new_order_id FROM dual;

-- 在addresses表中插入新地址
INSERT INTO addresses(address_id,user_id,name,address,phone) 
VALUES (address_seq.NEXTVAL,new_order_id,name,address,phone);
      
-- 遍历传入的产品ID和数量列表
FOR i IN prod_qty_array.first .. prod_qty_array.last LOOP
  
  -- 获取当前产品的库存数量
  SELECT quantity INTO current_quantity FROM products WHERE product_id = prod_qty_array(i)(1);        
  
  -- 如果库存充足，生成订单项并计算总价
  IF (current_quantity >= prod_qty_array(i)(2)) THEN
    
    total_price := total_price + (prod_qty_array(i)(2) * unit_price);
    
    INSERT INTO order_items(item_id, order_id, product_id,unit_price, quantity) 
    VALUES (order_item_seq.NEXTVAL, new_order_id, prod_qty_array(i)(1), unit_price,prod_qty_array(i)(2));
    
  -- 如果库存不足，抛出异常
  ELSE
    RAISE_APPLICATION_ERROR(-20001, 'Insufficient Quantity available for requested Product.');
  END IF;

END LOOP;

-- 返回总价
RETURN total_price;    
EXCEPTION
WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error: '||SQLERRM);
RETURN -1;

END create_order;
```

2. update_product_and_get_report 函数

此函数将接收以下参数:product_id、new_name、new_category和new_price。然后执行以下操作:

- 将产品表中指定的行更新为新值。
- 返回一份字符串，其中列出有关更新过程的信息：旧名称(如果更改），“价格增加/减少”， 或“类别从A转变为B”。

```sql
FUNCTION update_product_and_get_report (
product_id IN NUMBER,
new_name IN VARCHAR2 DEFAULT NULL,
new_category IN VARCHAR2 DEFAULT NULL,
new_price IN NUMBER DEFAULT -1
) RETURN VARCHAR2 IS

old_name VARCHAR2(50); 
old_category VARCHAR2(20); 
old_price NUMBER(10,2);  
price_diff NUMBER(10,2); 
report_str VARCHAR2(100):= '';
BEGIN

-- 获取旧的产品信息
SELECT product_name, category, price INTO old_name, old_category,old_price FROM products WHERE product_id = product_id FOR UPDATE;

-- 如果有新的名称，更新产品名字并添加到报告字符串中
IF new_name IS NOT NULL THEN
  report_str := 'Old name: ' || old_name ||'. ';
  UPDATE products SET product_name = new_name WHERE product_id = product_id;
  report_str := report_str || 'Name changed to: ' || new_name || '. ';
END IF;    

-- 如果有新的类别，更新产品类别并添加到报告字符串中
IF new_category IS NOT NULL THEN
  report_str := report_str || 'Category changed from: '|| old_category || ' to: ' || new_category ||'. ';
  UPDATE products SET category = new_category WHERE product_id = product_id;
END IF;

-- 如果有新的价格，更新产品价格并添加到报告字符串中，同时计算差价
IF (new_price >= 0 AND old_price <> new_price) THEN
    price_diff := new_price - old_price;
    report_str := report_str || CASE WHEN (price_diff > 0) THEN('Price increase by $'||TO_CHAR(price_diff)||'. ')
                                     ELSE ('Price decrease by $'||TO_CHAR(abs(price_diff)) ||'. ') 
                                END ;
    UPDATE products SET price = new_price WHERE product_id = product_id;
END IF;

-- 提交事务并返回报告字符串
COMMIT;
RETURN report_str;
EXCEPTION
WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error: '||SQLERRM);
RETURN NULL;

END update_product_and_get_report
```

3. clear_cart_items 存储过程

此存储过程将接收cart_id，从购物车项表cart_items中删除所有与该ID关联的行。

```sql
PROCEDURE clear_cart_items(
cart_id IN NUMBER
) AS

BEGIN

-- 删除与cart_id关联的购物车项
DELETE FROM cart_items WHERE cart_id = cart_id;
COMMIT;
EXCEPTION

WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error while clearing the cart_items');

END clear_cart_items;

END shopping_cart_pkg

```




## 数据库备份方案

1.创建目录（如果不存在）：
为备份文件提供一个目录，以确保备份文件的安全性和可靠性。
2.进行完全备份或差异备份：
周一到周五进行完全备份和差异备份；
完全备份：创建pfile备份，复制数据文件，使用ALTER DATABASE BEGIN BACKUP语句，确保我们的备份不会受到正在进行的数据库活动的影响；
差异备份：复制数据库归档文件。
3.压缩备份文件，并删除原始文件：
4.使用Linux系统的tar命令将备份文件压缩为.tar.gz格式，然后删除原始备份文件。该操作可以通过创建一个Shell脚本和使用DBMS_SCHEDULER.CREATE_JOB和DBMS_SCHEDULER.RUN_JOB来完成。
安全性：
本备份方案通过创建目录和使用DBMS_SCHEDULER.CREATE_JOB和DBMS_SCHEDULER.RUN_JOB进行备份和压缩，以确保数据的安全性和可靠性。同时，备份的目录应该受到严格的权限和访问控制，以确保只有有权限的用户才能访问备份文件。

**PL/SQL实现数据备份并打包成tar.gz的脚本:**

```sql
DECLARE

-- 定义一个变量，用于生成时间戳以在备份文件名中使用
v_timestamp varchar2(20):= TO_CHAR(SYSTIMESTAMP,'YYYY-MM-DD-HH24-MI-SS');

BEGIN

-- 创建目录（如果不存在）
BEGIN
EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY BACKUP_DIR AS ''/path/to/backups''';
EXCEPTION
WHEN OTHERS THEN
IF SQLCODE != -955 THEN
RAISE;
END IF;

END;

-- 周一到周五进行完全备份和差异备份
IF TO_CHAR(SYSDATE, 'D') >= 2 AND TO_CHAR(SYSDATE, 'D') <= 6 THEN

 -- 完全备份脚本
 EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
 EXECUTE IMMEDIATE 'CREATE PFILE=''backup.ora'', REUSE CURRENT CONTROLFILE';    
 UTL_FILE.Copy_Data( src_directory => 'BACKUP_DIR',src_filename=> 'backup.ora' , dest_directory=>'BACKUP_DIR',dest_file_name=>'pfile_backup_'||v_timestamp);     
ELSE
-- 差异备份脚本
UTL_FILE.Copy('BACKUP_DIR','archive')
END IF;

-- 压缩备份文件，并删除原始文件
BEGIN

DBMS_SCHEDULER.CREATE_JOB (
job_name             => 'compress_full_diff_backups',
job_type             => 'EXECUTABLE',
job_action           => 'tar -zcvf /path/to/backups/full_diff_backup_'|| v_timestamp || '.tar.gz /path/to/backups/.dbf /path/to/backups/pfile_backup_; rm /path/to/backups/.dbf /path/to/backups/pfile_backup_;',
number_of_arguments  => 0,
start_date           => SYSTIMESTAMP,
repeat_interval      => NULL,
end_date             => NULL,
enabled              => FALSE,
comments             => 'Compress full and differential backups and clear intermediate files.'
);

-- 运行压缩备份任务
DBMS_SCHEDULER.RUN_JOB(
  JOB_NAME => 'compress_full_diff_backups'
);
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20001,SQLERRM);

END;

END;

```

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项     | 评分标准                             | 满分 |
| :--------- | :----------------------------------- | :--- |
| 文档整体   | 文档内容详实、规范，美观大方         | 10   |
| 表设计     | 表设计及表空间设计合理，样例数据合理 | 20   |
| 用户管理   | 权限及用户分配方案设计正确           | 20   |
| PL/SQL设计 | 存储过程和函数设计正确               | 30   |
| 备份方案   | 备份方案设计正确                     | 20   |



