-- 1. create_order 函数

-- 此函数将接收以下参数:name,address,phone,email,和prod_qty_array(单独的产品ID和相应数量的值列表)。然后处理传入的数据，并执行以下操作:

-- - 创建新的订单(order_id)
-- - 将新地址插入到addresses表
-- - 为每个商品生成新的order_item
-- - 返回总价格(total_price)

CREATE OR REPLACE PACKAGE body shopping_cart_pkg IS

FUNCTION create_order (
name IN VARCHAR2,
address IN VARCHAR2,
phone IN VARCHAR2,
email IN VARCHAR2,
prod_qty_array IN sys.odcinumberlist
) RETURN NUMBER IS

new_order_id NUMBER;
new_address_id NUMBER;
total_price NUMBER := 0;
current_quantity NUMBER:=0;
BEGIN

-- 生成新的订单ID
SELECT order_seq.NEXTVAL INTO new_order_id FROM dual;

-- 在addresses表中插入新地址
INSERT INTO addresses(address_id,user_id,name,address,phone) 
VALUES (address_seq.NEXTVAL,new_order_id,name,address,phone);
      
-- 遍历传入的产品ID和数量列表
FOR i IN prod_qty_array.first .. prod_qty_array.last LOOP
  
  -- 获取当前产品的库存数量
  SELECT quantity INTO current_quantity FROM products WHERE product_id = prod_qty_array(i)(1);        
  
  -- 如果库存充足，生成订单项并计算总价
  IF (current_quantity >= prod_qty_array(i)(2)) THEN
    
    total_price := total_price + (prod_qty_array(i)(2) * unit_price);
    
    INSERT INTO order_items(item_id, order_id, product_id,unit_price, quantity) 
    VALUES (order_item_seq.NEXTVAL, new_order_id, prod_qty_array(i)(1), unit_price,prod_qty_array(i)(2));
    
  -- 如果库存不足，抛出异常
  ELSE
    RAISE_APPLICATION_ERROR(-20001, 'Insufficient Quantity available for requested Product.');
  END IF;

END LOOP;

-- 返回总价
RETURN total_price;    
EXCEPTION
WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error: '||SQLERRM);
RETURN -1;

END create_order;

-- 2. update_product_and_get_report 函数

-- 此函数将接收以下参数:product_id、new_name、new_category和new_price。然后执行以下操作:

-- - 将产品表中指定的行更新为新值。
-- - 返回一份字符串，其中列出有关更新过程的信息：旧名称(如果更改），“价格增加/减少”， 或“类别从A转变为B”。

FUNCTION update_product_and_get_report (
product_id IN NUMBER,
new_name IN VARCHAR2 DEFAULT NULL,
new_category IN VARCHAR2 DEFAULT NULL,
new_price IN NUMBER DEFAULT -1
) RETURN VARCHAR2 IS

old_name VARCHAR2(50); 
old_category VARCHAR2(20); 
old_price NUMBER(10,2);  
price_diff NUMBER(10,2); 
report_str VARCHAR2(100):= '';
BEGIN

-- 获取旧的产品信息
SELECT product_name, category, price INTO old_name, old_category,old_price FROM products WHERE product_id = product_id FOR UPDATE;

-- 如果有新的名称，更新产品名字并添加到报告字符串中
IF new_name IS NOT NULL THEN
  report_str := 'Old name: ' || old_name ||'. ';
  UPDATE products SET product_name = new_name WHERE product_id = product_id;
  report_str := report_str || 'Name changed to: ' || new_name || '. ';
END IF;    

-- 如果有新的类别，更新产品类别并添加到报告字符串中
IF new_category IS NOT NULL THEN
  report_str := report_str || 'Category changed from: '|| old_category || ' to: ' || new_category ||'. ';
  UPDATE products SET category = new_category WHERE product_id = product_id;
END IF;

-- 如果有新的价格，更新产品价格并添加到报告字符串中，同时计算差价
IF (new_price >= 0 AND old_price <> new_price) THEN
    price_diff := new_price - old_price;
    report_str := report_str || CASE WHEN (price_diff > 0) THEN('Price increase by $'||TO_CHAR(price_diff)||'. ')
                                     ELSE ('Price decrease by $'||TO_CHAR(abs(price_diff)) ||'. ') 
                                END ;
    UPDATE products SET price = new_price WHERE product_id = product_id;
END IF;

-- 提交事务并返回报告字符串
COMMIT;
RETURN report_str;
EXCEPTION
WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error: '||SQLERRM);
RETURN NULL;

END update_product_and_get_report;

-- 3. clear_cart_items 存储过程

-- 此存储过程将接收cart_id，从购物车项表cart_items中删除所有与该ID关联的行。

PROCEDURE clear_cart_items(
cart_id IN NUMBER
) AS

BEGIN

-- 删除与cart_id关联的购物车项
DELETE FROM cart_items WHERE cart_id = cart_id;
COMMIT;
EXCEPTION

WHEN OTHERS THEN

ROLLBACK;
DBMS_OUTPUT.PUT_LINE('Error while clearing the cart_items');

END clear_cart_items;

END shopping_cart_pkg