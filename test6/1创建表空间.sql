/* 创建名为 data_tablespace 的表空间 */
CREATE TABLESPACE data_tablespace
DATAFILE 'dyt_sales_data01.dbf'
SIZE 500M
AUTOEXTEND ON;

/* 创建名为 index_tablespace 的表空间 */
CREATE TABLESPACE index_tablespace
DATAFILE 'dyt_sales_idx01.dbf'
SIZE 500M
AUTOEXTEND ON;