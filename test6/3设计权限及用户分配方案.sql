-- 创建两个角色，分别为销售经理和销售操作员
CREATE ROLE c##myapp_sales_manager_1;
CREATE ROLE c##myapp_sales_operator_1;

-- 创建用户 Alice，并授予销售经理角色，拥有管理数据表的所有权限
CREATE USER c##alice IDENTIFIED BY password123;
GRANT CONNECT, RESOURCE TO c##alice;
GRANT CREATE SESSION TO c##alice; -- 允许用户登录
GRANT UNLIMITED TABLESPACE TO c##alice; -- 不限制表空间大小
GRANT c##myapp_sales_manager_1 TO c##alice; -- 授予销售经理角色

-- 创建用户 Bob，并授予销售操作员角色，只能查看和修改部分字段
CREATE USER c##bob IDENTIFIED BY password456;
GRANT CONNECT, RESOURCE TO c##bob;
GRANT CREATE SESSION TO c##bob;
GRANT SELECT ON products TO c##bob; -- 允许查询商品信息表
GRANT INSERT, UPDATE (quantity) ON cart_items TO c##bob; -- 允许更改购物项数量
GRANT SELECT ON users TO c##bob ; -- 允许查询用户列表
GRANT c##myapp_sales_operator_1 TO c##bob; -- 授予销售操作员角色

-- 销售经理角色拥有从sales_data表空间读写相关表的权限
GRANT SELECT,INSERT,UPDATE,DELETE ON products TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON addresses TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON carts TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON cart_items TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON orders TO c##myapp_sales_manager_1;
GRANT SELECT,INSERT,UPDATE,DELETE ON order_items TO c##myapp_sales_manager_1;

-- 销售操作员角色拥有从sales_data表空间读取相关表的权限
GRANT SELECT ON products TO c##myapp_sales_operator_1;
GRANT SELECT ON addresses TO c##myapp_sales_operator_1;
GRANT SELECT ON users TO c##myapp_sales_operator_1;
GRANT SELECT, INSERT, UPDATE(quantity) ON cart_items TO c##myapp_sales_operator_1;
GRANT SELECT, INSERT, UPDATE(unit_price,quantity) ON order_items TO c##myapp_sales_operator_1;