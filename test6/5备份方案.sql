DECLARE

-- 定义一个变量，用于生成时间戳以在备份文件名中使用
v_timestamp varchar2(20):= TO_CHAR(SYSTIMESTAMP,'YYYY-MM-DD-HH24-MI-SS');

BEGIN

-- 创建目录（如果不存在）
BEGIN
EXECUTE IMMEDIATE 'CREATE OR REPLACE DIRECTORY BACKUP_DIR AS ''/path/to/backups''';
EXCEPTION
WHEN OTHERS THEN
IF SQLCODE != -955 THEN
RAISE;
END IF;

END;

-- 周一到周五进行完全备份和差异备份
IF TO_CHAR(SYSDATE, 'D') >= 2 AND TO_CHAR(SYSDATE, 'D') <= 6 THEN

 -- 完全备份脚本
 EXECUTE IMMEDIATE 'ALTER DATABASE BEGIN BACKUP';
 EXECUTE IMMEDIATE 'CREATE PFILE=''backup.ora'', REUSE CURRENT CONTROLFILE';    
 UTL_FILE.Copy_Data( src_directory => 'BACKUP_DIR',src_filename=> 'backup.ora' , dest_directory=>'BACKUP_DIR',dest_file_name=>'pfile_backup_'||v_timestamp);     
ELSE
-- 差异备份脚本
UTL_FILE.Copy('BACKUP_DIR','archive')
END IF;

-- 压缩备份文件，并删除原始文件
BEGIN

DBMS_SCHEDULER.CREATE_JOB (
job_name             => 'compress_full_diff_backups',
job_type             => 'EXECUTABLE',
job_action           => 'tar -zcvf /path/to/backups/full_diff_backup_'|| v_timestamp || '.tar.gz /path/to/backups/.dbf /path/to/backups/pfile_backup_; rm /path/to/backups/.dbf /path/to/backups/pfile_backup_;',
number_of_arguments  => 0,
start_date           => SYSTIMESTAMP,
repeat_interval      => NULL,
end_date             => NULL,
enabled              => FALSE,
comments             => 'Compress full and differential backups and clear intermediate files.'
);

-- 运行压缩备份任务
DBMS_SCHEDULER.RUN_JOB(
  JOB_NAME => 'compress_full_diff_backups'
);
EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20001,SQLERRM);

END;

END;